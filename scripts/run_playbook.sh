#!/bin/bash

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

inventory=${1:-'hosts.yml'}
playbook=$2

if [ -n "$ADMINUSER_PRIVATE_KEY" ]; then
  eval "$(ssh-agent -s)"
  cat "$ADMINUSER_PRIVATE_KEY" | ssh-add -
fi

mkdir -p ~/.ssh && chmod 700 ~/.ssh
cat << EOF > ~/.ssh/config
Host *
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
    ForwardAgent yes
EOF

echo -e "============================================="
echo -e "Playbook: ${GREEN}${playbook}${NC}"
echo -e "============================================="

ansible-playbook -i ${inventory} --vault-password-file $ANSIBLE_PASSWORD_FILE ${playbook}.yml
