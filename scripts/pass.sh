#!/bin/bash

password=$1
name=${2:-"password"}

docker run --rm -v $PWD/password-file:/password-file \
  registry.gitlab.com/scrollfinance/infra/docker/ansible:latest \
  ansible-vault encrypt_string --vault-id password-file $password --name $name
